package com.hivin.mytsa

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import kotlinx.android.synthetic.main.item_films.view.*


class TMDBViewHolder(itemView: View, private val onFilmsListener: TMDBAdapter.OnFilmsListener) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
    private val itemFilm: TextView = itemView.item_films
    private val itemFilmSuite: TextView = itemView.item_films_suite
    init {
        itemView.setOnClickListener(this)
    }


    fun display(FilmsList: FilmsList) {
        itemFilm.text = FilmsList.title
        itemFilmSuite.text = FilmsList.overview

        //val poster: ImageView = itemView.findViewById(R.id.item_films_poster)


        /*Glide.with(itemView)
            .load("https://image.tmdb.org/t/p/w342${FilmsList.poster_path}")
            .transform(CenterCrop())
            .into(poster)*/


    /*GlideApp.with(itemView)
    .load("https://image.tmdb.org/t/p/w342${FilmsList.poster_path}")
    .into(poster) */
}

override fun onClick(v: View?) {
onFilmsListener.onFilmsClick(adapterPosition)
}

}
