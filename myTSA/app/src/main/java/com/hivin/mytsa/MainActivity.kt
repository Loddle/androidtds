package com.hivin.mytsa

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity(), TMDBAdapter.OnFilmsListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val builder = Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(GsonConverterFactory.create())

        val page = (0..150).random()
        val retrofit = builder.build()
        val client = retrofit.create(TMDBClient::class.java)
        val call = client.getFilmsConnus(page = page)

        call.enqueue(object: Callback<GetFilmsResponse> {
            override fun onResponse(call: Call<GetFilmsResponse>, response: Response<GetFilmsResponse>)
            {
                val list = response.body()
                Log.d("Repository", "FilmMain: ${list!!.films}")
                myRecyclerView.layoutManager = LinearLayoutManager(this@MainActivity,LinearLayoutManager.HORIZONTAL, false)
                myRecyclerView.adapter = TMDBAdapter(list!!.films, this@MainActivity)
                (myRecyclerView.adapter as TMDBAdapter)!!.updateFilms(list!!.films)

        }

            override fun onFailure(call: Call<GetFilmsResponse>, t: Throwable)
            {
                Toast.makeText(this@MainActivity, "Error...!!!", Toast.LENGTH_SHORT).show()
                println(t)
            }

        })
    }

    override fun onFilmsClick(position: Int) {
        val i = Intent(Intent.ACTION_VIEW)

        if (i.resolveActivity(packageManager) != null) {
            startActivity(i)
        }
    }

}
