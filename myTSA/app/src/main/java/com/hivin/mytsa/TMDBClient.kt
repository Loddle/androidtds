package com.hivin.mytsa

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface TMDBClient {

    @GET("movie/popular")
    fun getFilmsConnus(
        @Query("api_key") apiKey: String = "d1cb9dc4f9b0dca0d01a6e1b1e82fdb8",
        @Query("page") page: Int
    ): Call<GetFilmsResponse>

}
