package com.hivin.mytsa

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView



class TMDBAdapter(private var listFilms: ArrayList<FilmsList>, private val onFilmsListener: OnFilmsListener) :
    RecyclerView.Adapter<TMDBViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TMDBViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_films, parent, false)
        return TMDBViewHolder(view, onFilmsListener)
    }

    override fun onBindViewHolder(holder: TMDBViewHolder, position: Int) {
        holder.display(listFilms[position])
    }

    override fun getItemCount() = listFilms.size

    fun updateFilms(films: ArrayList<FilmsList>) {
        Log.d("Repository", "Film: $films")
        this.listFilms = films
    }

    interface OnFilmsListener{
      fun onFilmsClick(position: Int)
    }


}

