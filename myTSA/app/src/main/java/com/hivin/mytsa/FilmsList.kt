package com.hivin.mytsa

import com.google.gson.annotations.SerializedName


data class FilmsList (
    @SerializedName("id") val id: Long,
    @SerializedName("title") val title: String,
    @SerializedName("overview") val overview: String,
    @SerializedName("poster_path") val poster_path: String,
    @SerializedName("backdrop_path") val backdrop_path: String,
    @SerializedName("vote_average") val vote_average: Float,
    @SerializedName("release_date") val release_date: String
)

data class GetFilmsResponse(
    @SerializedName("page") val page: Int,
    @SerializedName("results") val films: ArrayList<FilmsList>,
    @SerializedName("total_pages") val pages: Int
)