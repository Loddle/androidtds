package com.hivin.newslist

import android.content.Intent
import android.os.Bundle
import android.net.Uri
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_news.*
import kotlinx.android.synthetic.main.activity_news.toolbar

class NewsActivity : AppCompatActivity() {

    private var login : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)
        setSupportActionBar(toolbar)

        toolbar.title = "News Activity"

        login = (applicationContext as NewsListApplication).login
        Toast.makeText(this, "Bienvenue $login", Toast.LENGTH_SHORT).show()

        button_details.setOnClickListener {
            val intent = Intent(this, DetailsActivity::class.java)
            startActivity(intent)
        }

        button_sortie.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        button_autre.setOnClickListener {
            val url = "http://android.busin.fr/"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(intent)
        }

    }
}

