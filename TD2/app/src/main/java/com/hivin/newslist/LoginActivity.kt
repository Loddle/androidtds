package com.hivin.newslist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_news.*


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setSupportActionBar(toolbar)
        toolbar.title = "Login Activity"


        button_entree.setOnClickListener {

            val pseudo = edit_pseudo.text.toString()
            if (pseudo != "") {

                (applicationContext as NewsListApplication).login = pseudo

                val intent = Intent(this, NewsActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Entrez votre pseudo", Toast.LENGTH_LONG).show()
            }
        }
    }
}
