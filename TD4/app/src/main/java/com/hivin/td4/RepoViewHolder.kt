package com.hivin.td4

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_repo.view.*

class RepoViewHolder(itemView: View, val onRepoListener: RepoAdapter.OnRepoListener) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
    private val itemRepo: TextView = itemView.itemRepo

    init {
        itemView.setOnClickListener(this)
    }

    fun display(RepoList: RepoList) {
        itemRepo.text = RepoList.name
    }

    override fun onClick(v: View?) {
        onRepoListener.onRepoClick(adapterPosition)
    }

}