package com.hivin.td4

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class RepoAdapter(private val listRepo: List<RepoList>, private val onRepoListener: OnRepoListener) :
    RecyclerView.Adapter<RepoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_repo, parent, false)
        return RepoViewHolder(view, onRepoListener)
    }

    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {
        holder.display(listRepo[position])
    }

    override fun getItemCount() = listRepo.size

    interface OnRepoListener{
        fun onRepoClick(position: Int)
    }
}