package com.hivin.td4

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity(), RepoAdapter.OnRepoListener {

    var list : ArrayList<RepoList>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val builder = Retrofit.Builder()
            .baseUrl("https://api.github.com")
            .addConverterFactory(GsonConverterFactory.create())

        val retrofit = builder.build()
        val client = retrofit.create(GitHubClient::class.java);
        val call = client.UserRepositories("JakeWharton")


        call.enqueue(object: Callback<ArrayList<RepoList>> {
            override fun onResponse(call: Call<ArrayList<RepoList>>, response: Response<ArrayList<RepoList>>)
            {
                list = response.body()

                myRecyclerView.layoutManager = LinearLayoutManager(this@MainActivity)
                myRecyclerView.adapter = RepoAdapter(list!!, this@MainActivity)
            }

            override fun onFailure(call: Call<ArrayList<RepoList>>, t: Throwable)
            {
                Toast.makeText(this@MainActivity, "Error...!!!", Toast.LENGTH_SHORT).show()
            }

        })
    }

    override fun onRepoClick(position: Int) {
        val i = Intent(Intent.ACTION_VIEW)
        i.setData(Uri.parse(list!![position].html_url))

        if (i.resolveActivity(packageManager) != null) {
            startActivity(i)
        }
    }

}

